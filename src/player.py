import sys
import os
import vlc
import time

np = "Now Playing: N/A"
status = 0
Instance = vlc.Instance()
p = Instance.media_player_new()
volume = p.audio_get_volume()

def play(link):
   Media = Instance.media_new(str(link))
   Media.get_mrl()
   p.set_media(Media)
   p.play()
   print (str(link))
   while True:
       if (p.get_state() == vlc.State.Stopped) or (p.get_state() == vlc.State.Ended):
           np = "Now Playing: N/A"
           status = 0
           break
       else:
           continue

def pause():
   p.pause()
   print("toggle paused")

def stop():
   p.stop()
   print("stopped")

def state():
   return p.get_state()

def setVolume(v):
   p.audio_set_volume(v)

def getVolume():
   p.audio_get_volume()
