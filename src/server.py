from flask import Flask, render_template, request
import youtube as youtube
import player as player
import threading
import vlc

app = Flask(__name__)

@app.route('/')
def index():
  return render_template('template.html', status=player.status, np=player.np, volume=player.volume)

def stream(input):
    if ("https://youtube.com" in str(input)):
      player.play(youtube.youtube_stream_link(input))
    else:
      player.play(youtube.youtube_stream_link(youtube.youtube_search(input)))


@app.route('/', methods=['POST'])
def link():
  if request.form['btn'] == 'Go':
    input = request.form['link']
    player.status = 1
    if (input != ""):
      if ("https://youtube.com" in str(input)):
        player.np = 'Now Playing: ' + youtube.get_title(input)
      else:
        player.np = 'Now Playing: ' + youtube.get_title(youtube.youtube_search(input))

      t = threading.Thread(target=stream, args=[input])
      t.start()
    return render_template("template.html", status=player.status, np=player.np, volume=player.volume)
  elif request.form['btn'] == 'Pause':
    if player.state() ==  vlc.State.Paused:
      player.status = 1
    elif player.state() == vlc.State.Playing:
      player.status = 2
    player.pause()
    return ("",204)
  elif request.form['btn'] == 'Stop':
    player.np = 'Now Playing: N/A'
    player.status = 0
    player.stop()
    return ("",204)
  elif request.form['btn'] == 'Volume':
    player.setVolume(int(request.form['vol']))
    player.volume = int(request.form['vol'])
    return ("",204)
  #return render_template("template.html", status=player.status, np=player.np, volume=player.volume)

if __name__ == '__main__':
  app.run(debug=True, host='0.0.0.0', port=80)
