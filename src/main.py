import player as player
import youtube as youtube
import sys

listargv = []

def main():
   listargv.extend(sys.argv)
   listargv.remove('main.py')

   if ("https://" in listargv[0]):
      player.play(youtube.youtube_stream_link(listargv[0]))
   else:
      player.play(youtube.youtube_stream_link(youtube.youtube_search(listargv)))

main()
