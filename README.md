# Pi Juke Box 

**About:**

Author: Khoa Nguyen (aka leScepter)

Please refer to the official repo if there're any issues: https://nguyenrepo.space/akhoa.nv/PiJukeBox


**Current feature:**
- Play single youtube link
- Search youtube using multiple query
- Spinning vinyl (pretty cool to me)
- Fully functional player with play, pause, stop

**What's next:**
- Spotify integration
- Play playlist on youtube
- Local media player

**Installation:**
Please visit repo's wiki for more detail


**Dependencies:**
 - Any version of [Python](https://www.python.org/)
 - [VLC bindings](https://pypi.python.org/pypi/python-vlc)
 - [Google API Client](https://developers.google.com/api-client-library/python/)
 - [Pafy](https://pypi.python.org/pypi/pafy/0.5.4)
 - [LXML](http://lxml.de/)
 - [Flask](http://flask.pocoo.org/)


**Screenshots:**
![1366x768](https://git.nguyenrepo.space/akhoa.nv/PiJukeBox/raw/master/screenshots/PiJukeBox_ver1.1.png)